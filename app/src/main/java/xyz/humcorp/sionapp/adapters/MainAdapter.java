package xyz.humcorp.sionapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.google.android.material.button.MaterialButton;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import xyz.humcorp.sionapp.R;
import xyz.humcorp.sionapp.utils.core.IOnItemClickListener;
import xyz.humcorp.sionapp.entities.Project;

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {

    private Context mContext;
    private List<Project> mProjects;
    private IOnItemClickListener mListener;

    public MainAdapter(Context mContext, List<Project> mProjects, IOnItemClickListener listener) {
        this.mContext = mContext;
        this.mProjects = mProjects;
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.activity_main_item, parent, false);
        return new MainAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Project project = mProjects.get(position);
        holder.setClick(mListener, project);
        holder.adapter_project_item_name.setText(project.getProjectName());
        holder.adapter_project_item_description.setText(project.getProjectDescription());
        Glide.with(mContext).load(project.getUrlImage()).into(holder.adapter_project_item_img);
    }

    @Override
    public int getItemCount() {
        return mProjects.size();
    }

    public void addProjects(List<Project> projects) {
        mProjects = new ArrayList<>(projects);
        notifyDataSetChanged();
    }

    public void removeAt(int position) {
        mProjects.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mProjects.size());
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.adapter_project_item_name)
        TextView adapter_project_item_name;
        @BindView(R.id.adapter_project_item_description)
        TextView adapter_project_item_description;
        @BindView(R.id.adapter_project_item_delete)
        Button adapter_project_item_delete;
        @BindView(R.id.adapter_project_item_edit)
        Button adapter_project_item_edit;
        @BindView(R.id.adapter_project_item_img)
        ImageView adapter_project_item_img;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void setClick(IOnItemClickListener mListener, Project project) {
            adapter_project_item_edit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(project);
                }
            });
            adapter_project_item_delete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(project.getId());
                    removeAt(getAdapterPosition());
                }
            });
        }
    }
}
