package xyz.humcorp.sionapp.entities;

public class Project {
    private String id;
    private String projectName;
    private String projectDescription;
    private String urlImage;

    public Project(String id, String projectName, String projectDescription, String urlImage) {
        this.id = id;
        this.projectName = projectName;
        this.projectDescription = projectDescription;
        this.urlImage = urlImage;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getProjectDescription() {
        return projectDescription;
    }

    public void setProjectDescription(String projectDescription) {
        this.projectDescription = projectDescription;
    }
}
