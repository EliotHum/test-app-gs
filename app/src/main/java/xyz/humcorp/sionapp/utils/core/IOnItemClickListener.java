package xyz.humcorp.sionapp.utils.core;

public interface IOnItemClickListener {
    void onItemClick(Object object);
}
