package xyz.humcorp.sionapp.signup;

import xyz.humcorp.sionapp.utils.core.IBasePresenter;
import xyz.humcorp.sionapp.entities.User;

public interface ISignUpPresenter extends IBasePresenter {
    void handleRegister(boolean isEdit, String name, String lastName, String phone, String email, String password);

    void getUserProfile();

    void onSuccess(User user);
}
