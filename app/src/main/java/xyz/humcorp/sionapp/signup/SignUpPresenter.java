package xyz.humcorp.sionapp.signup;

import android.content.Context;

import xyz.humcorp.sionapp.signup.ui.ISignUpView;
import xyz.humcorp.sionapp.entities.User;

public class SignUpPresenter implements ISignUpPresenter {

    private SignUpReposity mReposity;
    private ISignUpView mView;

    public SignUpPresenter(ISignUpView view, Context context) {
        mView = view;
        mReposity = new SignUpReposity(context, this);
    }

    @Override
    public void handleRegister(boolean isEdit, String name, String lastName, String phone, String email, String password) {
        if (mView != null) {
            mView.showProgress();
            mView.hideContainer();
        }
        mReposity.handleRegister(isEdit, name, lastName,
                phone, email, password);
    }

    @Override
    public void getUserProfile() {
        if (mView != null) {
            mView.showProgress();
            mView.hideContainer();
        }
        mReposity.getUserProfile();
    }

    @Override
    public void onSuccess(User user) {
        if (mView != null) {
            mView.hideProgress();
            mView.showContainer();
            mView.providerUserProfile(user);
        }
    }

    @Override
    public void onSuccess(String message) {
        if (mView != null) {
            mView.hideProgress();
            mView.showSuccess(message);
        }
    }

    @Override
    public void onFailure(String message) {
        if (mView != null) {
            mView.hideProgress();
            mView.showContainer();
            mView.showError(message);
        }
    }
}
