package xyz.humcorp.sionapp.signup;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

//import com.google.firebase.database.DatabaseReference;
//import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.HashMap;

import xyz.humcorp.sionapp.utils.AppPreferences;
import xyz.humcorp.sionapp.entities.User;

public class SignUpReposity {

    private final static String TAG = SignUpReposity.class.getSimpleName();
    private Context mContext;
    private ISignUpPresenter mPresenter;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private AppPreferences mPreferences;

    public SignUpReposity(Context context, ISignUpPresenter signUpPresenter) {
        mContext = context;
        mPresenter = signUpPresenter;
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mPreferences = AppPreferences.getInstance(context);
    }

    public void handleRegister(boolean isEdit, String name, String lastName, String phone, String email, String password) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("name", name);
        data.put("lastname", lastName);
        data.put("phone", phone);
        User dataUserObject = new User(name, lastName, phone);
        if (!isEdit) {
            mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                FirebaseUser user = mAuth.getCurrentUser();
                                if (user != null)
                                    onRequestNewUser(user, dataUserObject);
                                else
                                    mPresenter.onFailure("Hubo un error, intente nuevamente.");
                            } else {
                                mPresenter.onFailure(task.getException().getMessage());
                            }
                        }
                    });
        } else {
            onRequestUserUpdate(dataUserObject);
        }
    }

    private void onRequestUserUpdate(User user) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("name", user.getName());
        data.put("lastname", user.getLastname());
        data.put("phone", user.getPhone());
        db.collection("users").document(mAuth.getCurrentUser().getEmail())
                .update(data)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mPreferences.setUser(user);
                        mPresenter.onSuccess("Perfil actualizado exitósamente");
                    }
                })
                .addOnFailureListener(e -> {
                    mPresenter.onFailure(e.getMessage());
                });
    }

    private void onRequestNewUser(@NonNull FirebaseUser user, User userData) {
        db.collection("users")
                .document(user.getEmail())
                .set(userData)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        userData.setUuid(user.getUid());
                        mPreferences.setUser(userData);
                        mPresenter.onSuccess((String) null);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mPresenter.onFailure(e.getMessage());
                    }
                });
    }

    public void getUserProfile() {
        String userEmail = mAuth.getCurrentUser().getEmail();
        db.collection("users").document(userEmail).get()
                .addOnCompleteListener(new OnCompleteListener<DocumentSnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentSnapshot> task) {
                        if (!task.isSuccessful()) {
                            mPresenter.onFailure("Hubo un error, intente nuevamente");
                            return;
                        }
                        DocumentSnapshot document = task.getResult();
                        if (document.exists()) {
                            User user = new User(mAuth.getCurrentUser().getUid(), (String) document.get("name"), (String) document.get("lastname"),
                                    (String) document.get("phone"));
                            mPreferences.setUser(user);
                            mPresenter.onSuccess(user);
                        } else
                            mPresenter.onFailure("Información no encontrada");
                    }
                })
                .addOnFailureListener(e -> {
                    mPresenter.onFailure(e.getMessage());
                });
    }
}
