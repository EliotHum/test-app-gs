package xyz.humcorp.sionapp.signin;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class SignInRepository {

    private FirebaseAuth mAuth;
    private Context mContext;
    private ISignInPresenter mPresenter;

    public SignInRepository(Context context, ISignInPresenter presenter) {
        mAuth = FirebaseAuth.getInstance();
        mContext = context;
        mPresenter = presenter;
    }

    public void handleLogin(String email, String password) {
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener((Activity) mContext, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            FirebaseUser user = mAuth.getCurrentUser();
                            if (user != null)
                                mPresenter.onSuccess(null);
                            else
                                mPresenter.onFailure("Hubo un error, intente nuevamente.");
                        } else
                            mPresenter.onFailure(task.getException().getMessage());
                    }
                });
    }
}
