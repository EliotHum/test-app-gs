package xyz.humcorp.sionapp.signin;

import xyz.humcorp.sionapp.utils.core.IBasePresenter;

public interface ISignInPresenter extends IBasePresenter {
    void handleLogin(String email, String password);
}
