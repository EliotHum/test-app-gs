package xyz.humcorp.sionapp.signin.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import xyz.humcorp.sionapp.main.MainActivity;
import xyz.humcorp.sionapp.R;
import xyz.humcorp.sionapp.signin.ISignInPresenter;
import xyz.humcorp.sionapp.signin.SignInPresenter;
import xyz.humcorp.sionapp.signup.ui.SignUpActivity;
import xyz.humcorp.sionapp.utils.Utils;

public class SignInActivity extends AppCompatActivity implements ISignInView {

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.container)
    ConstraintLayout container;
    @BindView(R.id.activity_signin_password)
    TextInputEditText activity_signin_password;
    @BindView(R.id.activity_signin_email)
    TextInputEditText activity_signin_email;
    private ISignInPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViews();
        setupVars();
    }

    private void setupVars() {
        mPresenter = new SignInPresenter(this, this);
    }


    private void setupViews() {
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.activity_signin_login_btn)
    public void handleLogin() {
        String email = activity_signin_email.getText().toString();
        String password = activity_signin_password.getText().toString();
        if (email.isEmpty()) {
            Utils.showMessage(this, "Introducir un correo electrónico");
            return;
        }
        if (password.isEmpty()) {
            Utils.showMessage(this, "Introducir una contraseña");
            return;
        }
        mPresenter.handleLogin(email, password);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showContainer() {
        container.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideContainer() {
        container.setVisibility(View.GONE);
    }

    @Override
    public void showError(String message) {
        Utils.showMessage(this, message);
        activity_signin_password.setText("");
    }

    @Override
    public void showSuccess(String message) {
        Utils.showMessage(this, message);
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
        finish();
    }

    @OnClick(R.id.activity_signin_register)
    public void onNavigateRegisterScreen() {
        startActivity(new Intent(this, SignUpActivity.class));
    }
}