package xyz.humcorp.sionapp.signin;

import android.content.Context;

import xyz.humcorp.sionapp.signin.ui.ISignInView;

public class SignInPresenter implements ISignInPresenter {

    private ISignInView mView;
    private SignInRepository mRepository;

    public SignInPresenter(Context context, ISignInView view) {
        mView = view;
        mRepository = new SignInRepository(context, this);
    }

    @Override
    public void handleLogin(String email, String password) {
        if (mView != null) {
            mView.showProgress();
            mView.hideContainer();
        }
        mRepository.handleLogin(email, password);
    }

    @Override
    public void onSuccess(String message) {
        if (mView != null) {
            mView.hideProgress();
            mView.showSuccess(message);
        }
    }

    @Override
    public void onFailure(String message) {
        if (mView != null) {
            mView.hideProgress();
            mView.showContainer();
            mView.showError(message);
        }
    }
}
