package xyz.humcorp.sionapp.main;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import xyz.humcorp.sionapp.adapters.MainAdapter;
import xyz.humcorp.sionapp.R;
import xyz.humcorp.sionapp.signin.ui.SignInActivity;
import xyz.humcorp.sionapp.signup.ui.SignUpActivity;
import xyz.humcorp.sionapp.utils.core.IOnItemClickListener;
import xyz.humcorp.sionapp.utils.Utils;
import xyz.humcorp.sionapp.entities.Project;

public class MainActivity extends AppCompatActivity implements IMainView, IOnItemClickListener, SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {

    @BindView(R.id.activity_main_rv)
    RecyclerView activity_main_rv;
    private MainAdapter mAdapter;
    @BindView(R.id.bottom_sheet_project)
    ConstraintLayout bottom_sheet_project;
    private BottomSheetBehavior bottomSheetBehavior;
    @BindView(R.id.bs_img)
    ImageView bs_img;
    @BindView(R.id.activity_bs_name)
    TextInputEditText activity_bs_name;
    @BindView(R.id.activity_bs_description)
    TextInputEditText activity_bs_description;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.sr_activity_main)
    SwipeRefreshLayout sr_activity_main;
    @BindView(R.id.bs_choose_confirm_btn)
    Button bs_choose_confirm_btn;

    private IMainPresenter mPresenter;
    private String projectId;
    private Uri resultUri;
    private List<Project> mProjects;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupViews();
        setupVars();
    }

    private void setupViews() {
        setContentView(R.layout.activity_main);
        Toolbar mToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        ButterKnife.bind(this);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clearBS();
                showBottomSheetProject();
            }
        });
    }

    private void setupVars() {
        activity_main_rv.setHasFixedSize(true);
        activity_main_rv.setNestedScrollingEnabled(true);
        activity_main_rv.setLayoutManager(new LinearLayoutManager(this));
        mAdapter = new MainAdapter(this, new ArrayList<>(), this);

        mPresenter = new MainPresenter(this, this);
        mPresenter.getProjects();
        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_project);

        bs_img.setDrawingCacheEnabled(true);
        bs_img.buildDrawingCache();
        sr_activity_main.setOnRefreshListener(this);
        setupSheetBehavior();
    }

    @Override
    public void providerProjects(List<Project> projects) {
        if (projects != null && projects.isEmpty())
            Utils.showMessage(this, "Sin proyectos. Agregue un proyecto");
        mProjects = new ArrayList<>(projects);
        mAdapter.addProjects(projects);
        activity_main_rv.setAdapter(mAdapter);
        sr_activity_main.setRefreshing(false);
    }

    @OnClick(R.id.bs_choose_img_btn)
    public void handleChooseImage() {
        CropImage.activity()
                .setAspectRatio(10, 5)
                .setFixAspectRatio(true)
                .start(this);
    }

    @OnClick(R.id.bs_choose_confirm_btn)
    public void handleProjectConfirmBtn() {
        String projectName = activity_bs_name.getText().toString();
        String projectDescription = activity_bs_description.getText().toString();
        if (projectName.isEmpty()) {
            Utils.showMessage(this, "Agregar el nombre del proyecto");
            return;
        }
        if (projectDescription.isEmpty()) {
            Utils.showMessage(this, "Agregar la descripción del proyecto");
            return;
        }
        if (projectId == null && resultUri == null) {
            Utils.showMessage(this, "Seleccione una imagen");
            return;
        }
        if (projectId != null && !projectId.isEmpty())
            mPresenter.handleNewProject(projectId, resultUri, projectName, projectDescription);
        else
            mPresenter.handleNewProject(null, resultUri, projectName, projectDescription);
    }

    @OnClick(R.id.activity_order_process_btn_close)
    public void handleBottomSheetClose() {
        hideBottomsheetProject();
    }

    public void showBottomSheetProject() {
        if (bottomSheetBehavior.getState() != BottomSheetBehavior.STATE_EXPANDED)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

    private void hideBottomsheetProject() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
        bs_choose_confirm_btn.setText("Añadir proyecto");
        projectId = null;
    }

    private void setupSheetBehavior() {
        bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View view, int i) {
                if (i == BottomSheetBehavior.STATE_DRAGGING)
                    bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            }

            @Override
            public void onSlide(@NonNull View view, float v) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main_menu, menu);
        MenuItem menuItem = menu.findItem(R.id.main_menu_search);
        SearchView searchView = (SearchView) menuItem.getActionView();
        EditText editText = ((EditText) searchView.findViewById(R.id.search_src_text));
        editText.setHint("Buscar proyectos..");
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.main_menu_logout:
                FirebaseAuth.getInstance().signOut();
                startActivity(new Intent(this, SignInActivity.class));
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                finish();
                break;
            case R.id.main_menu_profile:
                Intent intent = new Intent(this, SignUpActivity.class);
                intent.putExtra("PROFILE", true);
                startActivity(intent);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public void showContainer() {

    }

    @Override
    public void hideContainer() {

    }

    @Override
    public void showError(String message) {
        Utils.showMessage(this, message);
    }

    @Override
    public void showSuccess(String message) {
        Utils.showMessage(this, message);
        mPresenter.getProjects();
        clearBS();
        hideBottomsheetProject();
    }

    @Override
    public void onSuccessDelete(String message) {
        Utils.showMessage(this, message);
    }

    @Override
    public void showProgressUploadImage(String message) {
        Utils.showMessage(this, message);
    }

    private void clearBS() {
        activity_bs_name.setText("");
        activity_bs_description.setText("");
        resultUri = null;
        bs_img.setImageDrawable(getDrawable(R.drawable.ic_no_picture));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                resultUri = result.getUri();
                if (CropImage.isReadExternalStoragePermissionsRequired(this, resultUri))
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
                else
                    bs_img.setImageURI(resultUri);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                Utils.showMessage(this, error.getMessage());
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (resultUri != null && grantResults.length > 0 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                    grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                startCropImageActivity(resultUri);
                bs_img.setImageURI(resultUri);
            } else {
                Toast.makeText(this, "Acepte la solicitud de permisos para tener una mejor experiencia", Toast.LENGTH_LONG).show();
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA}, CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE);
                }
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .start(this);
    }

    @Override
    public void onBackPressed() {
        if (bottomSheetBehavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
            hideBottomsheetProject();
        else
            super.onBackPressed();
    }

    @Override
    public void onItemClick(Object object) {
        if (object instanceof Project) {
            // edit project
            Project p = (Project) object;
            projectId = p.getId();
            activity_bs_name.setText(p.getProjectName());
            activity_bs_description.setText(p.getProjectDescription());
            Glide.with(this).load(p.getUrlImage()).into(bs_img);
            bs_choose_confirm_btn.setText("Editar proyecto");
            showBottomSheetProject();
        } else {
            // delete project
            mPresenter.handleDeleteProject(((String) object));
        }
    }

    @Override
    public void onRefresh() {
        mPresenter.getProjects();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        String userInput = newText.toLowerCase().trim();
        List<Project> newProjectList = new ArrayList<>();

        for (Project project : mProjects) {
            if (project.getProjectName().toLowerCase().contains(userInput) ||
                    project.getProjectDescription().toLowerCase().contains(userInput))
                newProjectList.add(project);
        }

        mAdapter.addProjects(newProjectList);
        activity_main_rv.setAdapter(mAdapter);
        return true;
    }
}