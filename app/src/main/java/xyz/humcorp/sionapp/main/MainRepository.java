package xyz.humcorp.sionapp.main;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import xyz.humcorp.sionapp.entities.Project;

public class MainRepository {

    private Context mContext;
    private IMainPresenter mPresenter;
    private FirebaseFirestore db;
    private FirebaseAuth auth;
    private FirebaseStorage storage;
    private static final String TAG = MainRepository.class.getSimpleName();

    public MainRepository(Context context, IMainPresenter presenter) {
        mContext = context;
        mPresenter = presenter;
        db = FirebaseFirestore.getInstance();
        auth = FirebaseAuth.getInstance();
        storage = FirebaseStorage.getInstance();
    }

    public void handleNewProject(String proyectId, Object image, String projectName, String projectDescription) {
        HashMap<String, Object> data = new HashMap<>();
        data.put("project_name", projectName);
        data.put("project_description", projectDescription);

        if (image != null) {
            Uri file = Uri.fromFile(new File(((Uri) image).getPath()));
            StorageReference riversRef = storage.getReference().child("images/" + file.getLastPathSegment());
            UploadTask uploadTask = riversRef.putFile(file);
            uploadTask
                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception exception) {
                            mPresenter.onFailure("Intente nuevamente");
                        }
                    })
                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Task<Uri> urlTask = uploadTask
                                    .continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                        @Override
                                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                            if (!task.isSuccessful()) {
                                                throw task.getException();
                                            }
                                            return riversRef.getDownloadUrl();
                                        }
                                    })
                                    .addOnCompleteListener(new OnCompleteListener<Uri>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Uri> task) {
                                            if (task.isSuccessful()) {
                                                Uri downloadUri = task.getResult();
                                                data.put("url_image", downloadUri.toString());
                                                newProject(proyectId, data);
                                            } else
                                                mPresenter.onFailure("Intente nuevamente");
                                        }
                                    });
                        }
                    })
                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onProgress(@NonNull UploadTask.TaskSnapshot taskSnapshot) {
                            mPresenter.showMessage(String.format("Subiendo imagen, %5.2f MB transferidos", taskSnapshot.getBytesTransferred() / 1024.0 / 1024.0));
                        }
                    });
        } else
            newProject(proyectId, data);
    }

    private void newProject(String proyectId, HashMap<String, Object> data) {
        String userEmail = auth.getCurrentUser().getEmail();
        if (proyectId == null) {
            db.collection("users").document(userEmail)
                    .collection("projects")
                    .add(data)
                    .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                        @Override
                        public void onSuccess(DocumentReference documentReference) {
                            mPresenter.onSuccess("Registro exitoso");
                        }
                    })
                    .addOnFailureListener(e -> {
                        mPresenter.onFailure(e.getMessage());
                    });
        } else {
            db.collection("users").document(userEmail)
                    .collection("projects")
                    .document(proyectId)
                    .update(data)
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            mPresenter.onSuccess("El proyecto fue editado exitosamente");
                        }
                    })
                    .addOnFailureListener(e -> {
                        mPresenter.onFailure(e.getMessage());
                    });
        }
    }

    public void getProjects() {
        String userEmail = auth.getCurrentUser().getEmail();
        db.collection("users").document(userEmail)
                .collection("projects")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (!task.isSuccessful()) {
                            mPresenter.onFailure("Intente nuevamente");
                            return;
                        }
                        List<Project> projects = new ArrayList<>();
                        for (QueryDocumentSnapshot document : task.getResult()) {
                            Log.d(TAG, document.getId() + " => " + document.getData());
                            projects.add(new Project(document.getId(), document.getData().get("project_name").toString(), document.getData().get("project_description").toString(), document.getData().get("url_image").toString()));
                        }
                        mPresenter.onSuccess(projects);
                    }
                })
                .addOnFailureListener(e -> {
                    mPresenter.onFailure(e.getMessage());
                });
    }

    public void deleteProject(String projectId) {
        String userEmail = auth.getCurrentUser().getEmail();
        db.collection("users").document(userEmail).collection("projects").document(projectId)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mPresenter.onSuccessDelete("Eliminado exitósamente");
                    }
                })
                .addOnFailureListener(e -> {
                    mPresenter.onFailure(e.getMessage());
                });
    }
}
