package xyz.humcorp.sionapp.main;

import java.util.List;

import xyz.humcorp.sionapp.utils.core.IBaseView;
import xyz.humcorp.sionapp.entities.Project;

public interface IMainView extends IBaseView {
    void providerProjects(List<Project> projects);

    void onSuccessDelete(String message);

    void showProgressUploadImage(String message);
}
